import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class App extends JFrame{
    private JPanel panel1;
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JPanel parametersPanel;
    private JTextField examplesTF;
    private JTextField placesTF;
    private JTextField reservedTF;
    private JTextField occupyTF;
    private JButton firstFreeButton;
    private JButton go2n3Button;
    private JButton goN2Button;
    private JPanel graphPanel;

    private static App _instance = null;
    private DefaultCategoryDataset dataset;

    public App get_instance() {
        if (_instance == null) {
            _instance = new App();
        }
        return _instance;
    }

    App() {
        dataset = new DefaultCategoryDataset();

        setContentPane(panel1);
        setSize(1280, 920);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        JFreeChart lineChart = ChartFactory.createLineChart(
                "Parking probability",
                "Experiments","Probability",
                createDataset(),
                PlotOrientation.VERTICAL,
                true,true,false);


        /*ChartFrame chartFrame = new ChartFrame("Text", lineChart, true);
        chartFrame.setVisible(true);
        chartFrame.setSize(500, 400);*/

        ChartPanel chartPanel = new ChartPanel( lineChart);
        chartPanel.setSize(graphPanel.getWidth(), graphPanel.getHeight());

        graphPanel.setBackground(Color.CYAN);
        graphPanel.removeAll();
        graphPanel.add(chartPanel, BorderLayout.CENTER);
        graphPanel.updateUI();
        graphPanel.setVisible(true);
    }

    private DefaultCategoryDataset createDataset() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

        dataset.addValue( 15 , "schools" , "1970" );
        dataset.addValue( 30 , "schools" , "1980" );
        dataset.addValue( 60 , "schools" ,  "1990" );
        dataset.addValue( 120 , "schools" , "2000" );
        dataset.addValue( 240 , "schools" , "2010" );
        dataset.addValue( 300 , "schools" , "2014" );
        return dataset;
    }

    private void addToDataset(double x, double y, String key) {
        dataset.addValue( y , key , String.valueOf(x));
        updateChart();
    }

    private void updateChart() {
    }

    public static void main(String[] args) {
        App app = new App();
        app.pack();
    }
}
